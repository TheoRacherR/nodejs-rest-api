const User = require('../models/User')
const jwt = require('jsonwebtoken');
const jwtSecret = 'JNaZcAPqBr4dPqiMhwavDjZCgABfgtfhzedLJyjzregfraJukvoXGHi'

module.exports = {
  getAll: async (req, res) => {
    try {
      
      //synthaxe error
      // res.status(400).json( err.message )

      const users = await User.find()
      res.status(200).json(users)

      //non authentifié
      res.status(401).json( err.message )

      //non authorisé
      res.status(403).json( err.message )

      //rien trouvé
      res.status(404).json( err.message )
    }
    catch (err) {
      res.status(500).json( err.message )
    }

  },

  getOne: async (req, res) => {
    //200
    //400
    //401
    //403
    //404
    //500
    res.send("")
  },
  
  post: async (req, res) => {
    try {
      const jwt = req.header.jwt;
      jwt.verify(jwt, jwtSecret, {}, async (err, user) => {
        if (err) throw err;
        const { pass_id, first_name, last_name, age, phone_number, address } = req.body;
        if(pass_id === '' || first_name === '' || last_name === '' || age === '' || phone_number === '' || address === ''){
        res.status(400).json("some element missing")
        return
      }
      })
    }
    catch (err) {
      res.status(500).json( err.message )
    }
    //201
    //400
    //401
    //403
    //404
    //500    
  },

  getJWTTokenByUser: async (req, res) => {
    jwt.sign({id: req.body.userId}, jwtSecret, {}, (err, token) => {
      if(err) throw err;
      res.status(200).json("JWT Token: ", token);
    })
  },
  
  patch: async (req, res) => {
    //200
    //400
    //401
    //403
    //404
    //500
    res.send("")
  },
  
  delete: async (req, res) => {
    //200
    //400
    //401
    //403
    //404
    //500
    res.send("")
  },
  
  isUserHasPlace: async (req, res) => {

  },
  
}