const Place = require('../models/Place')

module.exports = {
  getAll: async (req, res) => {
    try {
      
      //synthaxe error
      // res.status(400).json( err.message )

      //non authentifié (sans jwt)
      // res.status(401).json( err.message )

      //non authorisé (sans role)
      // res.status(403).json( err.message )

      //rien trouvé
      if(places.length === 0) {
        res.status(404).json( err.message )
        return
      }

      const places = await Place.find()
      res.status(200).json(places)

    }
    catch (err) {
      res.status(500).json( err.message )
    }

  },

  getOne: async (req, res) => {
    //200
    //400
    //401
    //403
    //404
    //500
    res.send("")
  },

  getPlaceOfUser: async (req, res) => {

  },
  
  post: async (req, res) => {
    //201
    //400
    //401
    //403
    //404
    //500
    res.send("")
  },
  
  patch: async (req, res) => {
    //200
    //400
    //401
    //403
    //404
    //500
    res.send("")
  },
  
  delete: async (req, res) => {
    //200
    //400
    //401
    //403
    //404
    //500
    res.send("")
  },
  
}