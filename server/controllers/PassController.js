const Pass = require('../models/Pass')

module.exports = {
  getAll: async (req, res) => {
    try {
      
      //synthaxe error
      // res.status(400).json( err.message )

      const passes = await Pass.find()
      res.status(200).json(passes)

      //non authentifié
      res.status(401).json( err.message )

      //non authorisé
      res.status(403).json( err.message )

      //rien trouvé
      res.status(404).json( err.message )
    }
    catch (err) {
      res.status(500).json( err.message )
    }

  },

  getOne: async (req, res) => {
    //200
    //400
    //401
    //403 que par son user
    //404
    //500
    res.send("")
  },
  
  post: async (req, res) => {
    //201
    //400
    //401
    //403
    //404
    //500
    res.send("")
  },
  
  patch: async (req, res) => {
    //200
    //400
    //401
    //403
    //404
    //500
    res.send("")
  },
  
  delete: async (req, res) => {
    //200
    //400
    //401
    //403
    //404
    //500
    res.send("")
  },
  
}