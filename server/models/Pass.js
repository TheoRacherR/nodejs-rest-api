const mongoose = require('mongoose');
const { Schema } = mongoose


const PassSchema = Schema({
  level: {
    require: true,
    type: Number,
    range: { //vérifier
      min: 1,
      max: 5,
    }
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
})

const PassModel = mongoose.model('Pass', PassSchema);
module.exports = PassModel;