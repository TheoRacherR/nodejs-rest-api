const mongoose = require('mongoose');
const { Schema } = mongoose


const UserSchema = Schema({
  pass_id: [
    {
      type: Schema.Types.ObjectId, 
      ref: 'Pass',
      require: true
    }
  ],
  
  first_name: {
    type: String,
    require: true
  },

  last_name: {
    type: String,
    require: true
  },

  age: {
    type: Number,
    require: true
  },

  phone_number: {
    type: String,
    require: true
  },

  address: {
    type: String,
    require: true
  }
})

const UserModel = mongoose.model('User', UserSchema);
module.exports = UserModel;