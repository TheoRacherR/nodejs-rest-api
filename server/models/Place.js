const mongoose = require('mongoose');
const { Schema } = mongoose


const PlaceSchema = Schema({
  address: {
    type: String,
    require: true
  },
  phone_number: {
    type: String,
    require: true
  },
  required_pass_level: {
    type: String,
    require: true
  },
  required_age_level: {
    type: String,
    require: true
  },
})

const PlaceModel = mongoose.model('Place', PlaceSchema);
module.exports = PlaceModel;