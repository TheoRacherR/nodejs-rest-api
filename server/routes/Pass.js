const express = require('express');
const router = express.Router();

const controller = require("../controllers/PassController")


//Get all pass
router.get('/', controller.getAll)

//Get one pass
router.get('/:id', controller.getOne)

//Post one pass
router.post('/', controller.post)

//Update one pass
router.patch('/:id', controller.patch)

//Delete one pass
router.delete('/:id', controller.delete);


module.exports = router;