const express = require('express');
const router = express.Router();

const controller = require("../controllers/PlaceController")


//Get all place
router.get('/', controller.getAll)

//Get one place
router.get('/:id', controller.getOne)

//Post one place
router.post('/', controller.post)

//Update one place
router.patch('/:id', controller.patch)

//Delete one place
router.delete('/:id', controller.delete);


module.exports = router;