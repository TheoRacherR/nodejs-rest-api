const express = require('express');
const router = express.Router();

const controller = require("../controllers/UserController")


//Get all user
router.get('/', controller.getAll)

//Get one user
router.get('/:id', controller.getOne)

//Post one user
router.post('/', controller.post)

//Update one user
router.patch('/:id', controller.patch)

//Delete one user
router.delete('/:id', controller.delete);


module.exports = router;